Foreach ($path in [System.IO.Directory]::EnumerateFiles("C:\ProgramData\Microsoft\Wlansvc\Profiles", "*.xml", "AllDirectories")) {
    Try {
        $oXml = New-Object System.XML.XMLDocument;
        $oXml.Load($path);
        $ssid = $oXml.WLANProfile.SSIDConfig.SSID.name;
        $netinfo = netsh.exe wlan show profiles name="$ssid" key=clear;
        $pass = (($netinfo | Select-String -Pattern "Key Content") -split ":")[1].Trim();
        $sendData += "SSID: " + ($ssid) + "`n" + "PASSWORD: " + ($pass) + "`n`n";
    }
    Catch {}
}
Write-Host $sendData;